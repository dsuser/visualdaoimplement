namespace Transporte.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using BLL.Models;

    public partial class DBContext : DbContext
    {
        public DBContext() : base("name=DBContext")
        {
        }

        public virtual DbSet<PaisModel> Paises { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PaisModel>().Property(e => e.PaisNombre).IsUnicode(false);
        }
    }
}
