﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transporte.BLL.Implements;
using Transporte.BLL.Interfaces;
using Transporte.BLL.Models;

namespace Transporte
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void guardar_Click(object sender, RoutedEventArgs e)
        {
            //Acceso a datos
            PaisDAO dao = new PaisImplement();
            //Modelo al que se creara una entidad
            PaisModel pais = new PaisModel();
            //Set de datos
            pais.PaisNombre = nombrePais.Text;
            //Acciones a ejecutar
            dao.registrar(pais);
        }
    }
}
