﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transporte.BLL.Interfaces;
using Transporte.BLL.Models;
using Transporte.DAL;

namespace Transporte.BLL.Implements
{
    public class PaisImplement : PaisDAO
    {
       
        public void actualizar(PaisModel obj)
        {
            var context = new DBContext();
            context.Paises.AddOrUpdate(obj);
            context.SaveChanges();

        }

        public void eliminar(PaisModel obj)
        {
            var context = new DBContext();
            context.Paises.Remove(obj);
            context.SaveChanges();
        }

        public List<PaisModel> listadoPaises()
        {
            throw new NotImplementedException();
        }

        public PaisModel obtenerPorId(int id)
        {
            var context = new DBContext();
            PaisModel pais = context.Paises.First(p => p.PaisID == id );
            return pais;
        }

        public void registrar(PaisModel obj)
        {
            var context = new DBContext();
            context.Paises.Add(obj);
            context.SaveChanges();
        }
    }
}
