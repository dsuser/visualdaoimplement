﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transporte.BLL.Models
{
    [Table("Pais")]
    public class PaisModel
    {
        [Key]
        public int PaisID { get; set; }

        [StringLength(50)]
        public string PaisNombre { get; set; }
    }
}
