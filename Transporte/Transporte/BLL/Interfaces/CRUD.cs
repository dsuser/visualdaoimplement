﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transporte.BLL.Interfaces
{
    public interface CRUD<T>
    {
        List<T> listadoPaises();
        T obtenerPorId(int id);
        void registrar(T obj);
        void actualizar(T obj);
        void eliminar(T obj);
    }
}
